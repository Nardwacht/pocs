// maakt het speelveld aan
function fieldManager() {
    for( i = 0; i < 9; i++) {
        $("#field").append("<div class='field' id='" + i + "'></div>");
        $("#" + i).css({
            "background": "white",
            "border": "#686868 3px solid",
            "width": "30vw",
            "height": "30vw",
            "display":"inline-block",
            "margin-left":"-4px",
            "margin-bottom":"-7px",
            "padding":"0"
        });
        console.log(turn);
        if(turn === 0) {
            $("#speler1").fadeIn(200);
        }
        else {
            $("#speler2").fadeIn(200);
        }
        console.log("dikke");
    }
}

//decides whose turn it is.
function playerTurn() {
    if(turn === 0) {
        turn = 1;
        $("#speler1").fadeOut(200);
        window.setTimeout(function(){ 
            $("#speler2").fadeIn(200);
        }, 500);
    }
    else {
        turn = 0;
        $("#speler2").fadeOut(200);
        window.setTimeout(function(){ 
            $("#speler1").fadeIn(200);
        }, 500);
        
    }
}

//check if a tile was hit.
function hitScan(t, v) {
    if(field[v] === undefined) {
        field[v] = t;
    
        if(t == 1) {
            $("#"+v).append("<div class='circle'></div>");

        }
        else {
            $("#"+v).append("<div class='cross'></div>");
        }
        hasWon();
        //tie();
        playerTurn();
    } 
    else {
        hasWon();
        console.log(t + "IS aan zet..");
    }
    
}

//check if the game has finished due to a tie.
function tie() {
    if($.inArray(undefined, field !== -1) && !finished) {
        alert("Helaas... Gelijkspel!");
        window.setTimeout(function() {
            location.reload();
        },500);  
    }
}

//check who has won
function hasWon() {
    if($.inArray(undefined, field !== -1)) {
        if( field[0]  === 0 &&  field[1]  === 0 &&  field[2]  === 0 ||
            field[3]  === 0 &&  field[4]  === 0 &&  field[5]  === 0 ||
            field[6]  === 0 &&  field[7]  === 0 &&  field[8]  === 0 ||
            field[0]  === 0 &&  field[3]  === 0 &&  field[6]  === 0 ||
            field[1]  === 0 &&  field[4]  === 0 &&  field[7]  === 0 ||
            field[2]  === 0 &&  field[5]  === 0 &&  field[8]  === 0 ||
            field[0]  === 0 &&  field[4]  === 0 &&  field[8]  === 0) {
                window.setTimeout(function() {
                        if(confirm("Speler 1 heeft gewonnen!!! Nog een keer spelen?")) {
                            window.location.reload();
                        }
                }, 200);
                $(".field").off();
                finished = true;
            }
        else if(
            field[0]  === 1 &&  field[1]  === 1 &&  field[2]  === 1 ||
            field[3]  === 1 &&  field[4]  === 1 &&  field[5]  === 1 ||
            field[6]  === 1 &&  field[7]  === 1 &&  field[8]  === 1 ||
            field[0]  === 1 &&  field[3]  === 1 &&  field[6]  === 1 ||
            field[1]  === 1 &&  field[4]  === 1 &&  field[7]  === 1 ||
            field[2]  === 1 &&  field[5]  === 1 &&  field[8]  === 1 ||
            field[0]  === 1 &&  field[4]  === 1 &&  field[8]  === 1) {
                window.setTimeout(function() {
                    if(confirm("Speler 2 heeft gewonnen!!! Nog een keer spelen?")) {
                        window.location.reload();
                    }   
                }, 200);     
                $(".field").off();  
                finished = true;
            }           
        }   
    }
    