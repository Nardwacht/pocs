<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script
  src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
  integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
  crossorigin="anonymous"></script>
    <script src="fn.js"></script>
    <script src="script.js"></script>
    <!-- <script src="functions.js"></script> -->
    <link rel="stylesheet" href="style.css">
    <title>Document</title>
</head>
<body>
    <!-- <img src="kat.jpg" alt="kat"> -->
    <div id="canvas"> 
		<p id="titel"><span id="speler1" hidden>Beurt van Speler 1</span><span id="speler2" hidden>Beurt van Speler 2</span></p>
		<a href="#"></a>
    </div>
   <div id="field">
       
   </div>
   <div id="playAgain">
        <h2 id="gewonnen"></h2>
        
   </div>
</body>
</html>